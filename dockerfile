# 以nginx镜像为基础
FROM nginx

# 将构建好的静态文件复制到nginx中
COPY /dist /usr/share/nginx/html

# 将配置文件复制到nginx中
COPY nginx.conf /etc/nginx/conf.d/default.conf

# 暴露80端口
EXPOSE 80

# 启动nginx服务
CMD ["nginx", "-g", "daemon off;"]
